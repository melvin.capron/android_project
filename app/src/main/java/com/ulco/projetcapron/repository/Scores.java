package com.ulco.projetcapron.repository;

import com.ulco.projetcapron.objects.Score;

import java.util.ArrayList;

public class Scores {
    private static Scores instance;
    private ArrayList<Score> scores;

    /**
     * Private for Singleton.
     */
    private Scores(){

    }

    /**
     *
     * @return instance of scores
     */
    public static Scores getInstance(){
        if(instance == null){
            instance = new Scores();
        }

        return instance;
    }

    /**
     *
     * @return all scores
     */
    public ArrayList<Score> getScores() {
        return scores;
    }

    /**
     *
     * @param scores to be set
     */
    public void setScores(ArrayList<Score> scores) {
        this.scores = scores;
    }

    /**
     *
     * @param categoryName needed
     * @return a score by a category name
     */
    public Score getScoreByCategoryName(String categoryName){
        for(Score score : this.getScores()){
            if(score.getCategoryName().equals(categoryName)){
                return score;
            }
        }

        return null;
    }
}
