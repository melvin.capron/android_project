package com.ulco.projetcapron.repository;

import com.ulco.projetcapron.objects.Category;

import java.util.ArrayList;

public class Categories {
    private static Categories instance;
    private ArrayList<Category> categories;

    /**
     * Private for Singleton.
     */
    private Categories(){

    }

    /**
     *
     * @return instance of categories
     */
    public static Categories getInstance(){
        if(instance == null){
            instance = new Categories();
        }

        return instance;
    }

    /**
     *
     * @return all categories
     */
    public ArrayList<Category> getCategories() {
        return categories;
    }

    /**
     *
     * @param categories to be set
     */
    public void setCategories(ArrayList<Category> categories) {
        this.categories = categories;
    }

    /**
     *
     * @param categoryName needed
     * @return category based on the name
     */
    public Category getCategoryByName(String categoryName){
        for(Category category : this.getCategories()){
            if(category.getCategory().equals(categoryName)){
                return category;
            }
        }

        return null;
    }
}
