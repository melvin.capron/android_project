package com.ulco.projetcapron.intents;

import android.os.Bundle;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.ulco.projetcapron.R;
import com.ulco.projetcapron.component.CategoryListViewComponent;

import java.io.IOException;

public class ScoreActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);

        ListView listView = findViewById(R.id.listScores);

        try {
            //set the list view of scores
            listView.setAdapter(CategoryListViewComponent.getSourceListCategoriesWithScore(this));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}