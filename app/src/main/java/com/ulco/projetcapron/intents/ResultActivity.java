package com.ulco.projetcapron.intents;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.ulco.projetcapron.MainActivity;
import com.ulco.projetcapron.R;
import com.ulco.projetcapron.config.Config;
import com.ulco.projetcapron.converter.IConverter;
import com.ulco.projetcapron.converter.JsonConverter;
import com.ulco.projetcapron.objects.Score;
import com.ulco.projetcapron.reader.FileReaderPr;
import com.ulco.projetcapron.reader.IReader;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;

public class ResultActivity extends AppCompatActivity {
    public static String USER_SCORE_EXTRA = "user_score";
    public static String TOTAL_SCORE_EXTRA = "total_score";
    public static String CATEGORY_NAME_EXTRA = "category_name";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.result_activity);

        //get result from previous intent
        Intent intent = getIntent();

        int score = intent.getIntExtra(USER_SCORE_EXTRA, 0);
        int totalScore = intent.getIntExtra(TOTAL_SCORE_EXTRA, 0);
        String categoryName = intent.getStringExtra(CATEGORY_NAME_EXTRA);

        TextView finalNote = findViewById(R.id.final_note);

        //set the final note
        finalNote.setText(
                finalNote.getText()
                        .toString()
                        .replace("%1", String.valueOf(score))
                        .replace("%2", String.valueOf(totalScore))
        );

        try {
            //create the score object and save it
            Score scoreObject = new Score(score, categoryName);
            save(scoreObject);
        } catch (IOException e) {
            e.printStackTrace();
        }

        prepareExitButton();
    }

    /**
     * Prepare exit button.
     */
    private void prepareExitButton() {
        Button exitButton = findViewById(R.id.exit);

        exitButton.setOnClickListener(v -> {
            Intent mainActivityIntent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(mainActivityIntent);
        });
    }

    /**
     *
     * @param score to be saved
     * @throws IOException
     */
    private void save(Score score) throws IOException {
        ArrayList<Score> scores = this.loadScores();

        boolean updated = false;

        for (Score scoreLoaded : scores) {
            //check if the score must be updated (it exists or not)
            if (scoreLoaded.getCategoryName().equals(score.getCategoryName())) {
                updated = true;
                scoreLoaded.setScore(score.getScore());
            }
        }

        //if does not exist yet, create it
        if (!updated) {
            scores.add(score);
        }

        IReader reader = new FileReaderPr();
        IConverter converter = new JsonConverter();

        //convert score to json
        String data = converter.setTo(scores);

        //write in file
        reader.writeFile(this, Config.SCORE_FILE_NAME, data, FileReaderPr.JSON_EXT);
    }

    /**
     *
     * @return all scores
     * @throws IOException
     */
    private ArrayList<Score> loadScores() throws IOException {
        IReader reader = new FileReaderPr();
        IConverter converter = new JsonConverter();

        //read score file
        String source = reader.readFile(this, Config.SCORE_FILE_NAME + FileReaderPr.JSON_EXT);

        Type type = new TypeToken<ArrayList<Score>>() {
        }.getType();

        //get it as a json file
        ArrayList<Score> result = converter.getFrom(source, type);

        //return it
        return result != null ? result : new ArrayList<>();
    }
}