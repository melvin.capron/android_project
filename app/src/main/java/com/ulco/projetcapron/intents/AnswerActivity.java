package com.ulco.projetcapron.intents;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.ulco.projetcapron.MainActivity;
import com.ulco.projetcapron.R;
import com.ulco.projetcapron.objects.Answer;
import com.ulco.projetcapron.objects.Category;
import com.ulco.projetcapron.objects.Game;
import com.ulco.projetcapron.objects.Question;
import com.ulco.projetcapron.repository.Categories;

public class AnswerActivity extends AppCompatActivity {
    public static int ANSWER_ACTIVITY = 1;
    public Category category;
    public Game game;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.answer_activity);

        //Return the data from the previous intent (category name)
        Intent intent = getIntent();
        String categoryName = intent.getStringExtra(MainActivity.CATEGORY);

        //load category with its questions
        this.category = Categories.getInstance().getCategoryByName(categoryName);
        //start a game
        this.game = new Game(this.category);

        //set the category name to the label
        ((TextView) findViewById(R.id.categoryName)).setText(categoryName);

        //load a question
        freshViewQuestion();
    }

    /**
     * Will load a question
     */
    private void freshViewQuestion() {
        //get next question based on the current number of answered questions
        Question question = this.game.getNextQuestion();

        //set the question label
        setQuestionTextView(question);

        //create all radio buttons
        final RadioGroup radioGroup = findViewById(R.id.answerList);

        //on click of a radio button, set the validate button as enabled
        radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
           if(group.getCheckedRadioButtonId() != -1){
               findViewById(R.id.validate).setEnabled(true);
           }
        });

        //show answers
        populateAnswers(question, radioGroup);

        //prepare validation button
        prepareButtonValidation(question, radioGroup);

        //prepare stop button
        prepareButtonStop();
    }

    /**
     *
     * @param question to be shown in the label
     */
    private void setQuestionTextView(Question question) {
        ((TextView) findViewById(R.id.question)).setText(question.getLabel());
    }

    /**
     * End the intent on stop press button.
     */
    private void prepareButtonStop() {
        Button stopButton = findViewById(R.id.stop);

        stopButton.setOnClickListener(v -> finish());
    }

    /**
     * On validation check answers
     *
     * @param question answered
     * @param radioGroup of answers
     */
    private void prepareButtonValidation(final Question question, final RadioGroup radioGroup) {
        Button validateButton = findViewById(R.id.validate);

        //Default behavior, button is disabled
        validateButton.setEnabled(false);

        //On click of the button...
        validateButton.setOnClickListener(v -> {
            //get the selected answer
            int selectedId = radioGroup.getCheckedRadioButtonId();
            RadioButton radioButton = findViewById(selectedId);

            //get the answer by its label
            Answer answer = question.getAnswerByLabel((String) radioButton.getText());

            //check if the answer is right
            if (answer.isRightAnswer()) {
                //increment the score
                game.addGoodAnswer();
            }

            //update the number of answered question
            game.addAnsweredQuestions();

            //if not ended yet, show the next question
            if (game.getAnsweredQuestions() < category.getQuestions().size()) {
                freshViewQuestion();
                return;
            }

            //otherwise, show the user the score result intent
            Intent resultActivityIntent = new Intent(getApplicationContext(), ResultActivity.class);
            resultActivityIntent.putExtra(ResultActivity.TOTAL_SCORE_EXTRA, category.getQuestions().size());
            resultActivityIntent.putExtra(ResultActivity.USER_SCORE_EXTRA, game.getGoodAnswers());
            resultActivityIntent.putExtra(ResultActivity.CATEGORY_NAME_EXTRA, category.getCategory());
            startActivity(resultActivityIntent);
        });
    }

    /**
     *
     * @param question to answer
     * @param radioGroup of answers
     */
    private void populateAnswers(Question question, RadioGroup radioGroup) {
        RadioButton radioButton;
        //clean the group
        radioGroup.removeAllViews();

        for (Answer answer : question.getAnswers()) {
            //for every question create a button
            radioButton = new RadioButton(this);
            radioButton.setText(answer.getLabel());
            radioGroup.addView(radioButton);
        }
    }
}