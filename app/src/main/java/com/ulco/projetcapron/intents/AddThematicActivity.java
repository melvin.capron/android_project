package com.ulco.projetcapron.intents;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.ulco.projetcapron.R;
import com.ulco.projetcapron.component.AdminManagerComponent;
import com.ulco.projetcapron.config.Config;
import com.ulco.projetcapron.converter.IConverter;
import com.ulco.projetcapron.converter.JsonConverter;
import com.ulco.projetcapron.objects.Answer;
import com.ulco.projetcapron.objects.Category;
import com.ulco.projetcapron.objects.Question;
import com.ulco.projetcapron.reader.FileReaderPr;
import com.ulco.projetcapron.reader.IReader;

import java.util.ArrayList;

public class AddThematicActivity extends AppCompatActivity {
    public static int MAX_ANSWERS_BY_QUESTION = 4;
    public static int MAX_QUESTIONS = 4;

    public int CURRENT_QUESTIONS;
    public int CURRENT_ANSWERS;

    private AdminManagerComponent answerComponent;

    private ArrayList<TableRow> addedAnswers;
    private ArrayList<Question> askedQuestions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_thematic);

        this.CURRENT_ANSWERS = 1;
        this.CURRENT_QUESTIONS = 1;
        this.addedAnswers = new ArrayList<>();
        this.askedQuestions = new ArrayList<>();

        //Component that will give all UI
        this.answerComponent = new AdminManagerComponent();
        LinearLayout tableLayout = findViewById(R.id.tableLayout);

        //Update visibility question number
        this.updateQuestionNumber();

        //Prepare buttons
        prepareAddQuestionButton(tableLayout);
        prepareAddAnswerButton(tableLayout);
        prepareEndButton();
    }

    /**
     * Will prepare the add question button
     *
     * @param tableLayout to update
     */
    private void prepareAddQuestionButton(LinearLayout tableLayout) {
        Button addQuestionButton = findViewById(R.id.addQuestion);

        addQuestionButton.setOnClickListener(view -> {
            //check if the max has been reeached
            if(this.CURRENT_QUESTIONS == MAX_QUESTIONS){
                Toast.makeText(this, "You cannot add more question than " + MAX_QUESTIONS, Toast.LENGTH_SHORT).show();
                return;
            }

            //on a new question, reset existing answer components
            for(TableRow r : this.addedAnswers){
                tableLayout.removeView(r);
                this.CURRENT_ANSWERS = 1;
            }

            try {
                //register the question
                registerQuestion(true);
            } catch (Exception e) {
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                return;
            }

            //update question number to keep tracking of the maximum
            this.CURRENT_QUESTIONS++;

            //set the new label
            this.updateQuestionNumber();

            //Reset form
            ((EditText) findViewById(R.id.base_answer)).setText("");
            ((EditText) findViewById(R.id.base_question)).setText("");
            ((CheckBox) findViewById(R.id.base_is_right_answer)).setChecked(false);
        });
    }

    /**
     * Will prepare the end button
     */
    private void prepareEndButton() {
        Button endButton = findViewById(R.id.end);

        endButton.setOnClickListener(view -> {
            try {
                //On end register last question done
                registerQuestion(false);
            } catch (Exception e) {
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                return;
            }

            //if there is not asked question, do not register yet
            if(this.askedQuestions.size() == 0){
                Toast.makeText(this, "You cannot send a thematic with no questions", Toast.LENGTH_SHORT).show();
                return;
            }

            try {
                //declaration of custom converter/reader
                IConverter jsonConverter = new JsonConverter();
                IReader fileReader = new FileReaderPr();

                //build category based on asked questions
                Category category = new Category(this.getThematicName(), this.askedQuestions);

                //convert them to json strong characters
                String data = jsonConverter.setTo(category);

                //then save them in a file
                fileReader.writeFile(this, this.getThematicName()+ Config.THEMATIC_SUFFIX, data, FileReaderPr.JSON_EXT);

                //notifty the user
                Toast.makeText(this, "Your thematic has been added successfully.", Toast.LENGTH_SHORT).show();

                //back to the main page
                finish();
            } catch (Exception e) {
                //catch any exception to notify the user
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Prepare add answer button.
     *
     * @param tableLayout to add answers to.
     */
    private void prepareAddAnswerButton(LinearLayout tableLayout) {
        Button addAnswerButton = findViewById(R.id.addAnswer);

        addAnswerButton.setOnClickListener(view -> {
            //keep tracking of max answers per question
            if(this.CURRENT_ANSWERS == MAX_ANSWERS_BY_QUESTION){
                Toast.makeText(this, "You cannot add more answers than " + MAX_QUESTIONS, Toast.LENGTH_SHORT).show();
                return;
            }

            //Add new entries of answer (combination of input and checkbox) dynamically
            TableRow tableRow = this.answerComponent.getAnswerComponent(this);

            //keep track to remove them after on new answer added
            addedAnswers.add(tableRow);

            tableLayout.addView(tableRow, tableLayout.getChildCount() - 1);

            //keep track
            this.CURRENT_ANSWERS++;
        });
    }

    /**
     * Register question for the given thematic
     *
     * @throws Exception to be thrown
     */
    private void registerQuestion(boolean strictCheck) throws Exception {
        //Retrieve question from component
        Question question = this.getQuestion(strictCheck);

        if(question.getLabel() == null || question.getLabel().equals("")){
            return;
        }

        askedQuestions.add(question);
    }

    /**
     * Get question
     *
     * @return a question based on the user input with all of its answers
     * @throws Exception to be thrown
     */
    private Question getQuestion(boolean strictCheck) throws Exception {
        EditText questionLabel = findViewById(R.id.base_question);

        if(questionLabel.getText().toString().equals("") && strictCheck){
            throw new Exception("You must name a question.");
        }

        return new Question(questionLabel.getText().toString(), this.getAnswers());
    }

    /**
     * Updates the label of question to know how much questions we have added yet
     */
    private void updateQuestionNumber() {
        TextView questionNumber = findViewById(R.id.question_number);
        questionNumber.setText(String.format(this.getString(R.string.question_n_1_d), this.CURRENT_QUESTIONS));
    }

    /**
     *
     * @return the thematic name from the user input
     * @throws Exception to be thrown
     */
    private String getThematicName() throws Exception {
        String thematicName = ((EditText) findViewById(R.id.thematic_name)).getText().toString();

        if(thematicName.equals("")){
            throw new  Exception("You must name a thematic.");
        }

        return thematicName;
    }

    /**
     *
     * @return all the answers based on the user inputs
     */
    private ArrayList<Answer> getAnswers(){
        ArrayList<Answer> answers = new ArrayList<>();
        TableLayout tableLayout = findViewById(R.id.tableLayout);

        //Loop over all the table layout
        for(int i = 0; i < tableLayout.getChildCount(); i++){
            View child = tableLayout.getChildAt(i);

            //if we're facing a table row
            if(child instanceof TableRow){
                TableRow tr = (TableRow) child;

                //and its first and second children are respectively an EditText (input) and CheckBox
                //it confirms that we are looking the answers
                if(tr.getChildAt(0) instanceof  EditText && tr.getChildAt(1) instanceof CheckBox){
                    EditText answerInput = (EditText) tr.getChildAt(0);
                    CheckBox isRightAnswerCheckBox = (CheckBox) tr.getChildAt(1);

                    //if the answer if blank or null, don't take it in account (no empty answers allowed)
                    if(answerInput.getText().toString() == null || answerInput.getText().toString().equals("")){
                        break;
                    }

                    //if everything seems to be ok, register it
                    Answer answer = new Answer(answerInput.getText().toString(), isRightAnswerCheckBox.isChecked());
                    answers.add(answer);
                }
            }
        }

        return answers;
    }
}