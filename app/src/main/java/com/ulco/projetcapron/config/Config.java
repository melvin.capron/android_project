package com.ulco.projetcapron.config;

public class Config {
    public static final String SCORE_FILE_NAME = "scores";
    public static final String THEMATIC_SUFFIX = "_thematic";
}
