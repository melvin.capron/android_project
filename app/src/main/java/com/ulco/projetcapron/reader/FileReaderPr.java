package com.ulco.projetcapron.reader;

import android.content.Context;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Objects;

/**
 * FileReaderPr is a reader for internal files
 */
public class FileReaderPr implements IReader {
    public static final String JSON_EXT = ".json";

    @Override
    public String readFile(Context context, String fileName) {
        //Get the text file
        File file = new File(context.getFilesDir(), fileName);

        //Read text from file
        StringBuilder text = new StringBuilder();

        try {
            //Read all
            BufferedReader br = new BufferedReader(new FileReader(file));
            String jsonValue;

            while ((jsonValue = br.readLine()) != null) {
                text.append(jsonValue);
            }

            br.close();
        } catch (IOException e) {
            //no
        }

        return text.toString();
    }

    @Override
    public ArrayList<String> readFolder(Context context, String pattern) {
        ArrayList<String> fileResult = new ArrayList<>();
        //List folder base
        String[] files = context.getFilesDir().list();

        for (String file : Objects.requireNonNull(files)) {
            if (file.contains(pattern)) {
                fileResult.add(this.readFile(context, file));
            }
        }

        return fileResult;
    }

    @Override
    public void writeFile(Context context, String fileName, String data, String extension) throws IOException {
        final File myFile = new File(context.getFilesDir(), fileName + extension);

        // create non existing
        myFile.createNewFile();

        // Save your stream, don't forget to flush() it before closing it.
        FileOutputStream fOut = new FileOutputStream(myFile);
        OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
        myOutWriter.append(data);

        myOutWriter.close();

        fOut.flush();
        fOut.close();

    }

    @Override
    public void removeFile(Context context, String fileName) {
        final File myFile = new File(context.getFilesDir(), "scores.json");

        myFile.delete();
    }
}
