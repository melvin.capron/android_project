package com.ulco.projetcapron.reader;

import android.content.Context;

import java.io.IOException;
import java.util.ArrayList;

public interface IReader {
    String readFile(Context context, String fileName) throws IOException;

    ArrayList<String> readFolder(Context context, String pattern) throws IOException;

    void writeFile(Context context, String fileName, String data, String extension) throws IOException;

    void removeFile(Context context, String fileName);
}
