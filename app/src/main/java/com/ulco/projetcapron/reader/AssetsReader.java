package com.ulco.projetcapron.reader;

import android.content.Context;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

/**
 * FileReaderPr is a reader for external files
 *
 * More explicitly, it allows an initiation and the versioning of custom questionnaries.
 */
public class AssetsReader implements IReader{
    public static final String STORED_FOLDER_JSON = "data";

    @Override
    public String readFile(Context context, String fileName) throws IOException {
        String jsonValue;

        //open all files in given folder
        InputStream inputStream = context.getAssets().open(String.format("%s/%s", STORED_FOLDER_JSON, fileName));

        int size = inputStream.available();

        byte[] buffer = new byte[size];

        //read it
        inputStream.read(buffer);
        inputStream.close();

        jsonValue = new String(buffer, StandardCharsets.UTF_8);

        //return the value
        return jsonValue;
    }

    @Override
    public ArrayList<String> readFolder(Context context, String folderName) throws IOException {
        ArrayList<String> fileResult = new ArrayList<>();
        //list all files from the folder name
        String[] files = context.getAssets().list(folderName);

        for (String file : files) {
            //add it as a result
            fileResult.add(this.readFile(context, file));
        }

        return fileResult;
    }

    @Override
    public void writeFile(Context context, String fileName, String data, String extension) {
        //do nothing, not used
    }

    @Override
    public void removeFile(Context context, String fileName) {
        //do nothing, not used
    }
}
