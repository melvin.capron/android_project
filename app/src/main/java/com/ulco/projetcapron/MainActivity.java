package com.ulco.projetcapron;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.ulco.projetcapron.component.CategoryListViewComponent;
import com.ulco.projetcapron.intents.AddThematicActivity;
import com.ulco.projetcapron.intents.AnswerActivity;
import com.ulco.projetcapron.intents.ScoreActivity;
import com.ulco.projetcapron.reader.FileReaderPr;
import com.ulco.projetcapron.reader.IReader;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {
    public static int MAIN_ACTIVITY = 2;
    public static final String CATEGORY = "category";
    IReader fileReader;
    private String customPassword = "MDP";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Internal file reader
        this.fileReader = new FileReaderPr();

        try {
            //update all category list
            populateCategoryList();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Init all buttons
        initScoreButton();
        initResetScoreButton();
        initAdminButton();
    }

    private void initAdminButton() {
        Button adminButton = findViewById(R.id.admin);

        adminButton.setOnClickListener(view -> {
            final EditText input = new EditText(this);
            input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            //Display a dialog box on click to check the password
            DialogInterface.OnClickListener dialogClickListener = (dialog, which) -> {
                String password = input.getText().toString();

                if (which == DialogInterface.BUTTON_POSITIVE && password.equals(this.customPassword)) {
                    //if the go button is pressed, change the intent
                    Intent intent = new Intent(getApplicationContext(), AddThematicActivity.class);
                    startActivityForResult(intent, MainActivity.MAIN_ACTIVITY);
                } else if(!password.equals(this.customPassword)){
                  Toast.makeText(this, "Please provide the good password", Toast.LENGTH_SHORT).show();
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Your password is required to access this part of the application.");


            builder.setView(input);

            builder.setPositiveButton("Go!", dialogClickListener).show();
        });
    }

    private void initResetScoreButton() {
        Button resetScoreButton = findViewById(R.id.resetScores);

        //Reset all score by a custom dialog box
        DialogInterface.OnClickListener dialogClickListener = (dialog, which) -> {
            if (which == DialogInterface.BUTTON_POSITIVE) {
                this.fileReader.removeFile(this, "scores.json");
                Toast.makeText(this, "Your score has been successfully cleaned", Toast.LENGTH_SHORT).show();
            }
        };

        resetScoreButton.setOnClickListener(view -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setMessage("Are you sure you want to reset all your scores?")
                    .setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();
        });
    }

    private void initScoreButton() {
        Button scoreButton = findViewById(R.id.displayScores);

        //Change the user activity to show his score
        scoreButton.setOnClickListener(view -> {
            Intent intent = new Intent(getApplicationContext(), ScoreActivity.class);
            startActivity(intent);
        });
    }

    /**
     * Will populate the list of category on the main page
     *
     * @return void
     */
    private void populateCategoryList() throws IOException {
        ListView listView = findViewById(R.id.listCategories);

        //array adapter to fill the category list with from a custom component (re-usable component)
        ArrayAdapter arrayAdapter = CategoryListViewComponent.getSourceListCategories(this);

        listView.setAdapter(arrayAdapter);

        //On item click, change the intent to start the questions
        listView.setOnItemClickListener((parent, view, position, id) -> {
            Intent activityManagerIntent = new Intent(getApplicationContext(), AnswerActivity.class);
            activityManagerIntent.putExtra(CATEGORY, (String) ((AppCompatTextView) view).getText());
            startActivityForResult(activityManagerIntent, AnswerActivity.ANSWER_ACTIVITY);
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            populateCategoryList();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}