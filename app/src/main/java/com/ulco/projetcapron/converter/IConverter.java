package com.ulco.projetcapron.converter;

import java.lang.reflect.Type;

public interface IConverter {
    <T> T  getFrom(String source, Type typeToken);

    String setTo(Object object);
}
