package com.ulco.projetcapron.converter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Type;

public class JsonConverter implements IConverter{
    @Override
    public <T> T  getFrom(String source, Type type) {
        Gson gson1 = new GsonBuilder()
                .serializeNulls()
                .disableHtmlEscaping()
                .setPrettyPrinting()
                .create();

        return gson1.fromJson(source, type);
    }

    @Override
    public String setTo(Object object) {
        Gson gson = new GsonBuilder()
                .serializeNulls()
                .disableHtmlEscaping()
                .setPrettyPrinting()
                .create();

        return gson.toJson(object);
    }
}
