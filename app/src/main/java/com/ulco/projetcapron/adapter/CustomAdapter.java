package com.ulco.projetcapron.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ulco.projetcapron.R;

import java.util.ArrayList;
import java.util.HashMap;

public class CustomAdapter extends BaseAdapter {
    ArrayList<String> result;
    Context context;
    ArrayList<String> score;
    private static LayoutInflater inflater = null;

    /**
     * This custom adapter allow the usage of a list view with 2 columns based on custom data
     *
     * @param mainActivity for the custom adapter
     * @param hashMap of results
     */
    public CustomAdapter(Context mainActivity, HashMap<String, String> hashMap) {
        result = new ArrayList<>(hashMap.keySet());
        context = mainActivity;
        score = new ArrayList<>(hashMap.values());
        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return result.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint({"SetTextI18n", "InflateParams"})
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder = new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.score_list, null);

        holder.categoryName = rowView.findViewById(R.id.listCategoryName);
        holder.scoreNumber = rowView.findViewById(R.id.listScoreValue);
        holder.categoryName.setText(result.get(position));
        holder.scoreNumber.setText(score.get(position).toString());

        return rowView;
    }

    public class Holder {
        TextView categoryName;
        TextView scoreNumber;
    }
}
