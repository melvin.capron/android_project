package com.ulco.projetcapron.objects;

public class Game {
    private final Category category;
    private int goodAnswers;
    private int answeredQuestions;

    /**
     *
     * @param category of the game launching
     */
    public Game(Category category){
        this.category = category;
        this.goodAnswers = 0;
        this.answeredQuestions = 0;
    }

    /**
     *
     * @return number of good answers
     */
    public int getGoodAnswers() {
        return goodAnswers;
    }

    /**
     * Adds a good answer to the count
     */
    public void addGoodAnswer() {
        this.goodAnswers++;
    }

    /**
     *
     * @return number of answered questions
     */
    public int getAnsweredQuestions() {
        return answeredQuestions;
    }

    /**
     * Increments the answered question number
     */
    public void addAnsweredQuestions() {
        this.answeredQuestions++;
    }

    /**
     *
     * @return the next question based on how much questions have been answered yet
     */
    public Question getNextQuestion(){
        return this.category.getQuestions().get(answeredQuestions);
    }
}
