package com.ulco.projetcapron.objects;

public class Score {
    private int score;
    private final String categoryName;

    /**
     *
     * @param score of the category
     * @param categoryName cn
     */
    public Score(int score, String categoryName){
        this.score = score;
        this.categoryName = categoryName;
    }

    /**
     *
     * @return the score of the category
     */
    public int getScore() {
        return score;
    }

    /**
     *
     * @param score of the category
     */
    public void setScore(int score) {
        this.score = score;
    }

    /**
     *
     * @return the category name
     */
    public String getCategoryName() {
        return categoryName;
    }
}
