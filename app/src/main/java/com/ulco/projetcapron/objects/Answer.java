package com.ulco.projetcapron.objects;

public class Answer {
    private final String label;
    private final boolean isRightAnswer;

    /**
     *
     * @param label of the answer
     * @param isRightAnswer is a good answer
     */
    public Answer(String label, boolean isRightAnswer) {
        this.label = label;
        this.isRightAnswer = isRightAnswer;
    }

    /**
     *
     * @return label of the answer
     */
    public String getLabel() {
        return label;
    }

    /**
     *
     * @return is a right answer
     */
    public boolean isRightAnswer() {
        return isRightAnswer;
    }
}
