package com.ulco.projetcapron.objects;

import java.util.ArrayList;

public class Question {
    private final String label;
    private final ArrayList<Answer> answers;

    /**
     *
     * @param label of the question
     * @param answers of the question
     */
    public Question(String label, ArrayList<Answer> answers) {
        this.label = label;
        this.answers = answers;
    }

    /**
     *
     * @return label of the question
     */
    public String getLabel() {
        return label;
    }

    /**
     *
     * @return answers of the question
     */
    public ArrayList<Answer> getAnswers() {
        return this.answers;
    }

    /**
     *
     * @param label of the answer needed
     * @return the answer if it does match
     */
    public Answer getAnswerByLabel(String label){
        for(Answer answer : this.answers){
            if(answer.getLabel().equals(label)){
                return answer;
            }
        }

        return null;
    }
}
