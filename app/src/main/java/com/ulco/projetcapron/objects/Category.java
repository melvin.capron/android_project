package com.ulco.projetcapron.objects;

import java.util.ArrayList;

public class Category {
    private final String category;
    private final ArrayList<Question> questions;

    /**
     *
     * @param category name
     * @param questions of the category
     */
    public Category(String category, ArrayList<Question> questions) {
        this.category = category;
        this.questions = questions;
    }

    /**
     *
     * @return category name
     */
    public String getCategory() {
        return category;
    }

    /**
     *
     * @return questions of the category category
     */
    public ArrayList<Question> getQuestions() {
        return questions;
    }
}
