package com.ulco.projetcapron.component;

import android.content.Context;
import android.text.InputType;
import android.view.Gravity;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TableRow;

import com.ulco.projetcapron.R;

public class AdminManagerComponent {
    /**
     *
     * @param context of the application
     * @return the answer component which is a combination of an EditText and CheckBox to add answers dynamically
     */
    public TableRow getAnswerComponent(Context context){
        TableRow tableRow = new TableRow(context);

        //EditText for the answer (input)
        EditText editText = new EditText(context);
        editText.setInputType(InputType.TYPE_CLASS_TEXT);
        editText.setHint(context.getString(R.string.answer_here));
        editText.setGravity(Gravity.CENTER);

        //CheckBox for the answer (is the answer right)
        CheckBox checkBox = new CheckBox(context);
        checkBox.setGravity(Gravity.CENTER);

        //Styling...
        TableRow.LayoutParams l = new TableRow.LayoutParams();
        l.gravity = Gravity.CENTER;
        checkBox.setLayoutParams(l);

        //Add it to the table row
        tableRow.addView(editText);
        tableRow.addView(checkBox);

        return tableRow;
    }
}
