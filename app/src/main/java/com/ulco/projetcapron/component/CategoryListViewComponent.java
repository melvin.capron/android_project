package com.ulco.projetcapron.component;

import com.ulco.projetcapron.adapter.CustomAdapter;
import com.ulco.projetcapron.config.Config;
import com.ulco.projetcapron.converter.IConverter;
import com.ulco.projetcapron.converter.JsonConverter;
import com.ulco.projetcapron.objects.Category;
import com.ulco.projetcapron.objects.Score;
import com.ulco.projetcapron.reader.AssetsReader;
import com.ulco.projetcapron.reader.FileReaderPr;
import com.ulco.projetcapron.reader.IReader;
import com.ulco.projetcapron.repository.Categories;
import com.ulco.projetcapron.repository.Scores;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.widget.ArrayAdapter;


public class CategoryListViewComponent {
    private static final Categories categories = Categories.getInstance();
    private static final Scores scores = Scores.getInstance();
    private static final List<String> sourceList = new ArrayList<>();
    private static ArrayAdapter<String> arrayAdapter = null;
    private static final HashMap<String, String> sourceListWithScores = new HashMap<String, String>();


    /**
     * @return the array adapter
     */
    public static ArrayAdapter<String> getSourceListCategories(Context context) throws IOException {
        sourceList.clear();
        //Load versionned categories (already written categories by the app, not created from it) to have
        //default thematics to answer
        ArrayList<Category> assetsCategories = loadCategoriesFromAssets(context);

        //Load custom categories which are created from the app
        ArrayList<Category> customCategories = loadCategoriesFromCustom(context);

        //Combine them
        assetsCategories.addAll(customCategories);

        categories.setCategories(assetsCategories);

        //Add them to the source list
        for (Category category : categories.getCategories()) {
            sourceList.add(category.getCategory());
        }

        //Return the array adapter for the list views
        arrayAdapter = new ArrayAdapter<>(
                context,
                android.R.layout.simple_list_item_1,
                sourceList
        );

        return arrayAdapter;
    }

    /**
     * @param context of the application
     * @return the array adapter with scores
     * @throws IOException to be thrown
     */
    public static CustomAdapter getSourceListCategoriesWithScore(Context context) throws IOException {
        //load scores properly from json

        loadScores(context);

        //for all categories, get their score back from the json
        for (Category category : categories.getCategories()) {
            Score score = scores.getScoreByCategoryName(category.getCategory());
            String preparedScore = score != null ? String.valueOf(score.getScore()) : "?";
            sourceListWithScores.put(category.getCategory(), preparedScore + "/" + category.getQuestions().size());
        }

        //This custom adapter allows a double column with the category name and the score
        CustomAdapter customAdapter = new CustomAdapter(
                context,
                sourceListWithScores
        );

        return customAdapter;
    }

    /**
     * @param context of the aplication
     * @return categories from assets (already existing ones)
     * @throws IOException to be thrown
     */
    private static ArrayList<Category> loadCategoriesFromAssets(Context context) throws IOException {
        IReader reader = new AssetsReader();
        IConverter converter = new JsonConverter();

        Type type = new TypeToken<ArrayList<Category>>() {
        }.getType();

        ArrayList<String> sources = reader.readFolder(context, AssetsReader.STORED_FOLDER_JSON);
        return converter.getFrom(String.valueOf(sources), type);
    }

    /**
     * @param context of the aplication
     * @return categories from internal storage (custom ones)
     * @throws IOException to be thrown
     */
    private static ArrayList<Category> loadCategoriesFromCustom(Context context) throws IOException {
        IReader reader = new FileReaderPr();
        IConverter converter = new JsonConverter();

        ArrayList<String> sources = reader.readFolder(context, Config.THEMATIC_SUFFIX);

        Type type = new TypeToken<ArrayList<Category>>() {
        }.getType();

        return converter.getFrom(String.valueOf(sources), type);
    }

    /**
     * @param context of the aplication
     * @throws IOException to be thrown
     */
    private static void loadScores(Context context) throws IOException {
        IReader reader = new FileReaderPr();
        IConverter converter = new JsonConverter();

        //read socre file
        String sources = reader.readFile(context, Config.SCORE_FILE_NAME + FileReaderPr.JSON_EXT);

        Type type = new TypeToken<ArrayList<Score>>() {
        }.getType();

        //convert it to an object
        converter.getFrom(String.valueOf(sources), type);

        //get the result
        ArrayList<Score> result = converter.getFrom(String.valueOf(sources), type);

        //set it as a score
        scores.setScores(result != null ? result : new ArrayList<>());
    }
}
